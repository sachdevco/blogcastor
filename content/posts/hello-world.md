---
title: Hello World
date: 2022-10-06T15:07:29+02:00
draft: false
description: Ceci est une description du Roi Castor
tags:
  - Castorius
categories:
  - Histoires
  - Saisons
---

# Le Roi des Castors

Je ne suis pas un homme mais un castor !

## Mon fief

Je passe tout mon temps à vivre autour des points d'eau

## Mon passe temps ?

Bloquer les cours d'eau !

![](/roi_castor.jpg)
